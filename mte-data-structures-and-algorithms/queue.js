let collection = [];

// Write the queue functions below.


//print
const print = () => {
	return collection;
};



//enqueue
const enqueue = (element) => {
	collection.push(element)
	return collection
};



//dequeue
const dequeue = (element) => {
	 if(isEmpty()) {
	 	return "Empty collection"
	 } else {
	 	collection.shift();
	 	return collection
	 }
};



// front
const front = () => {
	if(isEmpty()) {
	 	return "Empty collection";
	 } else {
	 	return collection[0];
	 }
}



// size
const size = () => {
	return collection.length
}



// isEmpty
const isEmpty = () => {
	return collection.length == 0
}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
 	front,
	size,
	isEmpty
};